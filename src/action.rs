use crate::room::Room;
use crate::item::{Item};
use crate::inventory::Inventory;


pub trait Go {
    fn go_north(current_room: &Room, moves: &mut Vec<String>) -> Option<String> {
        if current_room.north.is_some() {
            moves.push("north".to_string());
            return current_room.north.clone();
        } else {
            Room::invalid_direction();
            return None
        }
    }
    fn go_south(current_room: &Room, moves: &mut Vec<String>) -> Option<String> {
        if current_room.south.is_some() {
            moves.push("south".to_string());
            return current_room.south.clone();
        } else {
            Room::invalid_direction();
            return None
        }
    }
    fn go_east(current_room: &Room, moves: &mut Vec<String>) -> Option<String> {
        if current_room.east.is_some() {
            moves.push("east".to_string());
            return current_room.east.clone();
        } else {
            Room::invalid_direction();
            return None
        }
    }
    fn go_west(current_room: &Room, moves: &mut Vec<String>) -> Option<String> {
        if current_room.west.is_some() {
            moves.push("west".to_string());
            return current_room.west.clone();
        } else {
            Room::invalid_direction();
            return None
        }
    }
}

pub trait Pickup {
    fn pickup(&mut self, room: &mut Room, item_name: &Option<String>, items: &Vec<Item>);
}
pub trait Look {
    fn look(&self, player_inventory: &Inventory, rooms: &Vec<Room>, room: &Room, direction: &Option<String>);
    fn look_self(&self, player_inventory: &Inventory);
    fn look_direction(rooms: &Vec<Room>, room_direction: &str) {
        let room = Room::get_room_by_name(rooms, room_direction);
        if let Some(room) = room {
            println!("You see {}...", room.name);
        } else {
            // TODO: Duplicate code, better way to handle this?
            Room::nothing_to_see();
        }
    }
    fn look_north(rooms: &Vec<Room>, current_room: &Room) {
        if let Some(room) = &current_room.north {
            Self::look_direction(rooms, room);
        } else {
            Room::nothing_to_see();
        }
    }
    fn look_south(rooms: &Vec<Room>, current_room: &Room) {
        if let Some(room) = &current_room.south {
            Self::look_direction(rooms, room);
        } else {
            Room::nothing_to_see();
        }

    }
    fn look_east(rooms: &Vec<Room>, current_room: &Room) {
       if let Some(room) = &current_room.east {
            Self::look_direction(rooms, room);
        } else {
            Room::nothing_to_see();
        }

    }
    fn look_west(rooms: &Vec<Room>, current_room: &Room) {
        if let Some(room) = &current_room.west {
            Self::look_direction(rooms, room);
        } else {
            Room::nothing_to_see();
        }

    }
}

