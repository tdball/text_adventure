use serde::{Deserialize, Serialize};

use crate::action::{Pickup};
use crate::inventory::Inventory;
use crate::room::Room;
use crate::item::{Item};
use crate::index_of;


#[derive(Debug, Default, Serialize, Deserialize)]
pub struct Player {
    pub name: String,
    pub inventory: Inventory,
    pub score: i32,
    pub moves: Vec<String>,
    pub alive: bool,
}

impl Player {
    pub fn new(name: &str) -> Self {
        return Player {
            name: name.to_string(),
            inventory: Default::default(),
            score: 0,
            moves: Vec::new(),
            alive: true
        }
    }
    pub fn show_inventory(&self) {
        println!("You rifle through your bags...");
        println!("{}", self.inventory);
    }
    pub fn drop(&mut self, room: &mut Room, item_name: &Option<String>, items: &Vec<Item>) {
        if let Some(item_name) = item_name {
            if self.inventory.items.contains_key(item_name) {
                if let Some(item) = Item::get_by_name(&item_name, &items) {
                    self.inventory.items.remove(item_name);
                    room.items.push(item.name);
                }
            }
        }
    }
    pub fn destroy(&mut self, item_name: &Option<String>, items: &Vec<Item>) {
        if let Some(item_name) = item_name {
            if self.inventory.items.contains_key(item_name) {
                if let Some(item) = Item::get_by_name(&item_name, &items) {
                    self.inventory.items.remove(item_name);
                }
            }
        }
    }
}

impl Pickup for Player {
    fn pickup(&mut self, room: &mut Room, item_name: &Option<String>, items: &Vec<Item>) {
        if let Some(item_name) = item_name {
            if let Some(item) = Item::get_by_name(&item_name, &items) {
                if room.items.contains(&item_name.to_string()) {
                    self.inventory.items.insert(item_name.clone(), item.clone());
                    println!("You pickup: {}", &item_name);
                    self.score += 10;
                    let index = index_of(&item.name, &room.items);
                    room.items.remove(index);
                }
            }
        }
    }
}

