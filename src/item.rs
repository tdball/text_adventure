use serde::{Serialize, Deserialize};
use std::collections::HashMap;

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Item {
    pub name: String,
    pub description: String,
    pub light_source: bool,
    pub can_pickup: bool,
    pub scenario: Option<Event>
}

impl Item {
    pub fn get_by_name(item_name: &str, items: &Vec<Item>) -> Option<Item> {
        for item in items {
            if item_name == item.name {
                return Some(item.clone());
            }
        }
        return None;
    }
}

#[derive(Debug, Default, Deserialize, Serialize, Clone)]
pub struct Event {
    pub description: String,
    pub interacts_with: HashMap<String, Reaction>,
}

#[derive(Debug, Default, Deserialize, Serialize, Clone)]
pub struct Reaction {
    pub consumes: bool,
    pub response: String,
    pub updated_description: String,
}