use std::{io, process, thread};
use std::fs::File;
use std::io::Read;
use std::collections::HashMap;

use crate::room::Room;
use crate::item::{Item, };
use std::time::Duration;

pub struct Game;

impl Game {
    pub fn get_active_room_by_name<'john>(rooms: &'john Vec<Room>, room_name: &str) -> &'john Room {
        for room in rooms {
            if room_name == room.name {
                return room;
            }
        }
        println!("Did not find a room named: {}. Double check your assets/rooms.json file", room_name);
        println!("Teleporting you to the Start of the map");
        println!("===============");
        return &rooms[0];
    }
    pub fn get_active_room_by_name_as_clone(rooms: &Vec<Room>, room_name: &str) -> Option<Room> {
        for room in rooms {
            if room_name == room.name {
                return Some(room.clone());
            }
        }
        return None
    }
    pub fn get_player_input(message: Option<&str>) -> String {
        if let Some(message) = message {
            println!("{}", message);
        }
        let mut input = String::new();
        match io::stdin().read_line(&mut input) {
            Ok(_bytes_read) => {
                return input;
            }
            Err(error) => {
                println!("error: {}", error);
                panic!("Unable to handle error, exiting...");
            }
        }
    }
    pub fn call_action() {

    }
    pub fn debug_menu_loop(items: &Vec<Item>, rooms: &Vec<Room>) {
        println!("Entering Debug Menu");
        loop {
            let command = Game::get_player_input(None);
            let command = Game::parse_action(&command);
            let action = command.get("action").unwrap();
            let detail = command.get("detail").unwrap();
            if let Some(action) = action {
                match action.as_str() {
                    "list" => { Game::debug_list_handler(detail, items, rooms)}
                    "help" => {println!("Try \"list rooms\" or \"list armor\" etc...")}
                    "exit" => {println!("Leaving Debug Menu"); break;}
                    _ => {println!("I didn't quite understand \"{}\", type exit to leave", action.as_str())}
                }
            }
        }
    }
    fn debug_list_handler(detail: &Option<String>, items: &Vec<Item>, rooms: &Vec<Room>) {
        if let Some(detail) = detail {
            match detail.as_str() {
                "items" => {println!("{:#?}", items)}
                "rooms" => {println!("{:#?}", rooms)}
                _ => println!("No Resource found with that type")
            }
        }
    }
    pub fn parse_action(action: &String) -> HashMap<&str, Option<String>> {
        let mut parsed_action = action.to_string();
        parsed_action = parsed_action.trim().to_string();
        let split_action = parsed_action.split_whitespace();
        let mut actions: Vec<String> = Vec::new();
        for (index, action) in split_action.enumerate() {
            let mut action = String::from(action);
            if index == 0 {
                action = action.to_lowercase();
            }
            actions.push(String::from(action));
        }
        let mut commands = HashMap::new();
        actions.reverse();
        commands.insert("action", actions.pop());
        actions.reverse();
        if !actions.is_empty() {
            commands.insert("detail", Some(actions.join(" ")));
        } else {
            commands.insert("detail", None);
        }
        return commands;
    }
    fn load_file(path: &str) -> Option<String> {
        let file = File::open(path);
        match file {
            Ok(mut file) => {
                let mut contents = String::new();
                file.read_to_string(&mut contents);
                return Some(contents);
            },
            Err(error) => {
                println!{"error: {}", error}
            }
        }
        return None;
    }
    pub fn load_rooms() -> Vec<Room> {
        if let Some(raw_rooms) = Game::load_file("assets/rooms.json") {
            let rooms: Vec<Room> = serde_json::from_str(raw_rooms.as_str()).unwrap();
            return rooms;
        } else {
            panic!("Malformed JSON, invalid Rooms structure");
        }
    }
    pub fn load_items() -> Vec<Item> {
        if let Some(raw_items) = Game::load_file("assets/items.json") {
            let items: Vec<Item> = serde_json::from_str(raw_items.as_str()).unwrap();
            return items;
        } else {
            panic!("Malformed JSON, invalid Items Structure");
        }
    }

    pub fn exit() {
        println!("Thank you for playing!");
        process::exit(0);
    }

}



