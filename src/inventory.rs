use std::collections::HashMap;
use serde::{Deserialize, Serialize};
use crate::{item, room};

#[derive(Default, Debug, Deserialize, Serialize, Clone)]
pub struct Inventory {
    pub items: HashMap<String, item::Item>,
}

impl Inventory {
    pub fn equip() {}
    pub fn unequip() {}
    pub fn destroy(&mut self, name: &str) {
        if self.items.contains_key(name) {
            self.destroy_item(name);
        } else {
            println!{"Could not find {} in your inventory to drop!", name}
        }
    }
    pub fn drop(&mut self, name: &str, room: &mut room::Room) {
        if self.items.contains_key(name) {
            self.drop_item(name, room);
        } else {
            println!{"Could not find {} in your inventory to drop!", name}
        }
    }
    // TODO: Will expand functionality here later to account for quantity
    fn destroy_item(&mut self, name: &str) {
        self.items.remove(name);
    }
    fn drop_item(&mut self, name: &str, room: &mut room::Room) {
        let item = self.items.remove_entry(name).unwrap();
        room.items.push(item.0);
    }
}

impl std::fmt::Display for Inventory {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let mut message = String::new();
        message.push_str("Items:\n");
        for item in &self.items {
            message.push_str(format!(" - {}", item.0).as_str());
        }
        message.push_str("\n\n");
        write!(f, "{}", message)
    }
}
