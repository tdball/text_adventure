use std::{env};

use text_adventure::action::{Pickup, Look};
use text_adventure::game::Game;
use text_adventure::room::Room;
use text_adventure::player::Player;


fn main() {
    let args: Vec<String> = env::args().collect();
    println!("Args: {:#?}", args);
    let debug_mode = args.contains(&"--debug".to_string());

    // Setting up initial state.
    let items = Game::load_items();
    let mut rooms = Game::load_rooms();
    let mut player = Player::new("Trevor");
    let mut room_has_been_visited = false;
    let mut next_room_name: Option<String> = None;
//    let mut active_room = Game::get_active_room_by_name(&rooms, "Start");
    let mut active_room = Game::get_active_room_by_name_as_clone(&rooms, "Start");
    // Initial state complete

    loop {
        if debug_mode { println!("Moves Taken: {:#?}", &player.moves) }
        if debug_mode { println!("Player Score: {}", &player.score) }
        if let Some(name) = next_room_name {
            if debug_mode { println!("Loading Room: {:?} from next_room_name", &name) }
            active_room = Game::get_active_room_by_name_as_clone(&rooms, &name);
            // Clear next room as we move through the map
            next_room_name = None;
            // Reset visited to false, this only occurs if there's a next room to move to.
            room_has_been_visited = false;
        }

        // Initial visit, load description
        if !room_has_been_visited {
            if let Some(room) = &active_room {
                room.look_self(&player.inventory);
            }
//            active_room.look_self(&player.inventory);
            // This keeps us from echoing the description each time we process a verb-noun command
            room_has_been_visited = true;
        }
        // Get player input, and perform an action. If a room is returned, move to it

        let action = Game::get_player_input(None);
        let command = Game::parse_action(&action);
        println!();
        let detail = command.get("detail").unwrap();
        if let Some(action) = command.get("action").unwrap() {
            if debug_mode { println!("{:?}", command) }
            if let Some(room) = &mut active_room {
                match action.as_str() {
                    "debug" => { Game::debug_menu_loop(&items, &rooms) },
                    "go"|"g" => { next_room_name = Room::go_direction(room, detail, &mut player.moves) },
                    "look"|"l" => { room.look(&player.inventory, &rooms, &room, detail) },
                    "pickup"|"get"|"p" => { player.pickup(room, detail, &items) },
                    "drop"|"d" => { player.drop(room, detail, &items) },
                    "destroy" => { player.destroy(detail, &items) }
                    "bag"|"inventory"|"b"|"i" => { player.show_inventory() },
                    "quit"|"exit"|"q"|"e" => { Game::exit(); },
                    _ => { println!( "I have no idea how to \"{}\"!", action.as_str()) }
                }
                Room::update_state(&mut rooms, room.clone());
            }
        }
    }
}


