use serde::{Serialize, Deserialize};
use crate::action::{Go, Look};
use std::collections::HashMap;
use crate::inventory::Inventory;

#[derive(Clone, Default, Debug, Serialize, Deserialize)]
pub struct Room {
    pub name: String,
    pub description: String,
    pub look_description: String,
    pub items: Vec<String>,
    pub north: Option<String>,
    pub east: Option<String>,
    pub south: Option<String>,
    pub west: Option<String>,
}

impl Room {
    /// Returns an optional reference to a specific Room within the `Vec<Room>` struct, lives as
    /// long as the original Vec<Room> object, also ode to John here.
    pub fn get_room_by_name<'john>(rooms: &'john Vec<Room>, room_name: &str) -> Option<&'john Room>{
        for room in rooms {
            if room_name == room.name {
                return Some(room);
            }
        }
        return None;
    }

    pub fn invalid_direction() {
        println!("You can't go that direction!");
    }

    pub fn nothing_to_see() {
        println!("Nothing to see over there");
    }

    /// returns a hashmap of all possible exits to the provided &Room struct
    pub fn get_possible_exits(room: &Room) -> HashMap<&str, &String>{
        let mut exits = HashMap::new();
        if let Some(north) = &room.north {
            exits.insert("north", north);
        }
        if let Some(south) = &room.south {
            exits.insert("south", south);
        }
        if let Some(east) = &room.east {
            exits.insert("east", east);
        }
        if let Some(west) = &room.west {
            exits.insert("west", west);
        }
        return exits;
    }

    /// Updates the original loaded asset with the new items after pickup/drop
    pub fn update_state(rooms: &mut Vec<Room>, updated: Room) {
        for mut room in rooms {
            if room.name == updated.name {
                room.items = updated.items.clone();
            }
        }
    }

    /// Returns an optional room name, used to update the game loops `active_room` concept.
    pub fn go_direction(room: &Room, direction: &Option<String>, moves: &mut Vec<String>) -> Option<String> {
        if let Some(direction) = direction {
            match direction.as_str() {
                "north" | "n" => { return Room::go_north(room, moves); }
                "south" | "s" => { return Room::go_south(room, moves); }
                "east" | "e" => { return Room::go_east(room, moves); }
                "west" | "w" => { return Room::go_west(room, moves); }
                _ => {Room::invalid_direction()}
            }
        } else {
            println!("You can't just say go without a direction!")
        }
        return None
    }
}

/// Inherits default implementation.
impl Go for Room {}
/// Implementing ability to look in all directions
impl Look for Room {

    /// Abstraction handling looking in each direction, or at the current room if no direction
    /// is specified.
    fn look(&self, player_inventory: &Inventory, rooms: &Vec<Room>, room: &Room, direction: &Option<String>) {
        if let Some(direction) = direction {
            match direction.as_str() {
                "north" | "n" => { return Room::look_north(rooms, room) }
                "south" | "s" => { return Room::look_south(rooms, room) },
                "east" | "e" => { return Room::look_east(rooms, room) },
                "west" | "w" => { return Room::look_west(rooms, room) },
                _ => {Room::nothing_to_see()}
            }
        } else {
            self.look_self(player_inventory);
        }
    }

    /// Cross references the room inventory with the player inventory and only shows items that
    /// haven't already been picked up by the player. This limits the engine a bit, as the player
    /// can only pickup unique items, but this gets around the mutable borrows of a room in
    /// multiple places.
    fn look_self(&self, player_inventory: &Inventory) {
        println!("-= {} =-", self.name);
        println!("{}", self.description);
        println!("{}", self.look_description);
        println!();
        if !&self.items.is_empty() {
            let mut player_items = Vec::new();
            for item in player_inventory.items.keys() {
                player_items.push(item.clone())
            }
            let mut visible_items = Vec::new();
            for item in &self.items {
                if !player_items.contains(item) {
                    visible_items.push(item.clone());
                }
            }
            if !&visible_items.is_empty() {
                println!("You see some equipment laying on the ground...");
                for item in visible_items {
                    println!(" - {}", item);
                }
                println!();
            }
        }
        println!("Possible exits are:");
        for exit in Room::get_possible_exits(&self) {
            println!(" - {} : {}", exit.0, exit.1)
        }
    }
}