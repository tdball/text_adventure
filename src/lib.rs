pub mod room;
pub mod player;
pub mod game;
pub mod inventory;
pub mod item;
pub mod action;

pub fn index_of(value: &str, values: &Vec<String>) -> usize {
    return values.iter().position(|r| r == value).unwrap();
}
