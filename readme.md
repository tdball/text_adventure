# Text Adventures in Rust(WIP)
Mostly a learning exercise for myself. Goals are to write the engine to 
consume `.json` files containing the maps, items, etc...

by default the engine consumes the `rooms.json`, `items.json`, 
in the `assets` folder of the project. 
Feel free to adjust these following the examples within to alter the
game.
## Install
Get rust for your OS [here](https://rustup.rs/)

Build with the following commands:

```bash
cargo +stable build --release
```

## Usage
```bash
# Linux/Unix
./target/release/text_adventure 
# Windows
./target/release/text_adventure.exe
```

